#!/bin/bash
set -e

# setup ros environment
source "/opt/ros/$ROS_DISTRO/setup.bash"
source "/ds_sensors_ws/install/setup.bash"

exec "$@"
