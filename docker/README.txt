Author: Gregory Burgess (Caryn House - Blake Lab)
Email: gburgess@mit.edu
Date: 06/22/21

DOCKER FILES are for reference only and to share how we are using the DSL code and docker for the gliders. They were designed to work in a GitLab repo with the following file structure. Note: We are still developing are software and things may change. My goal is to create an avenue for the sentinel_aug repo (code base for slocum glider currently) to leverage existing code from the DSL codebase

docker/
	-->Dockerfile.sparton_m2_ahrs
	-->prod_entrypoint.sh
ds_sensors/
ds_msgs/
ds_base/
docker-compose.yml


I think the Dockerfile can be slightly modified to be a generic docker container able to run any sensor depending on how you write the docker-compose.yml file. Our example docker-compose.yml file, which was meant to spin up an isolated container for the Sparton-M2 AHRS is included for reference.



