#ifndef DS_SENSORS_SPARTONM2_PRIVATE_H
#define DS_SENSORS_SPARTONM2_PRIVATE_H

#include "ds_sensor_msgs/Ahrs.h"
#include "ds_sensor_msgs/VectorMagneticField.h"


namespace ds_sensors
{
struct SpartonM2Private
{
SpartonM2Private():
  imu_last{}, mag_last{}
  {}
  
  ~SpartonM2Private() = default;

  bool publish_rviz_marker_ = false;
  int rate_hz_ = 0;
  std::string protocol_ = "";

  ros::Publisher ahrs_pub_;
  ros::Publisher mag_pub_;
  ros::Publisher imu_pub_;
  ros::Publisher vis_pub_;

  sensor_msgs::Imu imu_last;
  ds_sensor_msgs::VectorMagneticField mag_last;

  enum VIDs
    {
      pitch = 8,
      roll = 9,
      yaw = 10,
      yawt = 11,
      magp = 23,
      accelp = 25,
      gyrop = 27,
      temperature = 120
    };

  
};
}

#endif  // DS_SENSORS_SPARTONM2_PRIVATE_H
