#include <tf/tf.h>
#include <tf/transform_datatypes.h>
#include <visualization_msgs/Marker.h>
#include "ds_sensors/spartonm2.h"
#include "spartonm2_private.h"
#include "math.h"

#include <sstream>

#define FMT std::hex << std::setw(2) << std::setfill('0')
#define STDGRAVITY 9.80665

// Cannot get ROS_ERROR macros to actually send to output="screen."  Giving up in favor of
// cout.  Change these macros once issue is resolved.
#define ERROR(a) std::cout << a
//#define ERROR(a) ROS_INFO_STREAM(a)
#define INFO(a) std::cout << a
//#define INFO(a) ROS_INFO_STREAM(a)
#define FATAL(a) std::cout << a; abort();
//#define FATAL(a) ROS_FATAL_STREAM(a)

// Redefining DEBUG to reduce verboseness of Sparton AHRS driver. Revert to cout definition if debugging is needed
//#define DEBUG(a)
#define DEBUG(a) std::cout << a
//#define DEBUG(a) ROS_DEBUG_STREAM(a)


namespace ds_sensors
{
SpartonM2::SpartonM2() : SensorBase(), d_ptr_(std::unique_ptr<SpartonM2Private>(new SpartonM2Private))
{
}

SpartonM2::SpartonM2(int argc, char* argv[], const std::string& name)
  : SensorBase(argc, argv, name), d_ptr_(std::unique_ptr<SpartonM2Private>(new SpartonM2Private))
{
}

SpartonM2::~SpartonM2() = default;

  std::pair<bool, ds_sensor_msgs::Ahrs> SpartonM2::parse_bytes(const ds_core_msgs::RawData& bytes)
{
    

  
}

void SpartonM2::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  auto mag = ds_sensor_msgs::VectorMagneticField{};  // spit this out at native rate.
  auto imu = sensor_msgs::Imu{};  // spit this out at native rate.
  auto ahrs = ds_sensor_msgs::Ahrs{};  // spit this at compass rate and populate internal fields with latest.

  //DEBUG("Got bytes: ");
  //for (int i=0; i<bytes.data.size(); i++)
  //  DEBUG(FMT << (int)bytes.data[i]);
  //DEBUG(std::endl);


  // Sparton compasses support multiple protocols, both ASCII and binary and
  // a highly configurable output.  This driver uses only the most basic, 
  // the pre-formatted output produced by the "mask" variables.
  // 
  // These don't seem to be implemented as settings that use the output
  // channels in "Bit Stream Ascii" mode.  @@@ Unclear how to completely
  // clear the communications settings (if for example unit was calibrated
  // using Sparton software).

  std::string buf(bytes.data.begin(),bytes.data.end());

  // Parse.  With all three basic "masks" on, the data is one of three messages:
  //
  // accelGyrop_mask d.on
  // Response example: [AGp,time_ms,accelp_X,accelp_Y,accelp_Z,gyrop_X,gyrop_Y,gyrop_Z,temp]
  // AGp,154261571, 171.02, 618.89,-751.83,2.425e-02,2.691e-02,-9.859e-03, 26.55
  //
  // compass_mask d.on
  // Response example: [C,time_ms, pitch_deg,roll_deg,yaw_deg]
  // C,11590797, 17.890,-43.611,231.619
  //
  // magp_mask d.on
  // Response example: [magp,time_ms,magp_X,magp_Y,magp_Z]
  // magp,11636044, -415.8, 192.3, 425.0
  //
  // @@@ perhaps also log the raw versions of AG and mag.  There are undocumented
  // mask variables for these that follow the same naming convention.

  // @@@ ... these don't all arrive at the same time, and they are off by ms.
  // each sub-message does have its own timestamp, but they also do not
  // arrive at the same rate.  compass output is much slower.  options:
  // 1. start everything on a compass message and timestamp the sub-components with the
  //    latest messages of the other type.
  // 2. produce a separate message for each data type at nominally native rate.
  // 3. spew mostly empty AHRS messages.
  // 4. retain the last of each type of message, updating the data and timestamp only when new data
  //    of that type arrives.
  //
  // BitStreamAscii protocol.  The output is determined at setup in this driver.  The current
  // format (2018-08-01) is:
  // $CUS0,
  DS_D(SpartonM2);

  if (d->protocol_ == "mask")
    {
      double roll_deg, pitch_deg, heading_deg, temperature = 0.0;
      if (buf.compare(0,2,"C,") == 0)
	{
	  DEBUG("Got compass message: " << buf);
	  // On this data create a complete AHRS message.
	  double yaw_deg = 0.0;
	  int n = sscanf(buf.c_str(),"C,%*d,%lf,%lf,%lf",
			 &pitch_deg,
			 &roll_deg,
			 &yaw_deg);
	  heading_deg = yaw_deg;  // yaw or yawt (true)?  See pg 13 of S/W interface guide.
	  
	  // populate AHRS message with processed result and last readings from high-rate outputs.
	  // Ignoring possible startup transient.
	  ahrs.roll = roll_deg;
	  ahrs.pitch = pitch_deg;
	  ahrs.compass.heading = heading_deg;
	  ahrs.compass.is_true_heading = true; // @@@ unknown!
	  ahrs.compass.heading_covar = ds_sensor_msgs::Compass::COMPASS_NO_DATA;
	  ahrs.imu = d->imu_last;
	  ahrs.mag = d->mag_last;
	  
	  ahrs.ds_header = bytes.ds_header;
	  FILL_SENSOR_HDR_IOTIME(ahrs, ahrs.ds_header.io_time);
	  
	  // orientation in IMU message is not populated.
	  double yaw_ros = -(ahrs.compass.heading - 90.0);
	  tf::Quaternion q = tf::createQuaternionFromRPY(ahrs.roll*M_PI/180.0,-ahrs.pitch*M_PI/180.0,yaw_ros*M_PI/180.0);
	  tf::quaternionTFToMsg(q, ahrs.imu.orientation); // @@@ this is weird because orientation is from this msg, not IMU.
	  
	  d->ahrs_pub_.publish(ahrs);
	  updateTimestamp("ahrs", ahrs.header.stamp);
	  
	  // Publish data for visualization if configured to do so.
	  // This gets published at the AHRS rate.
	  if (d->publish_rviz_marker_)
	    {
	      visualization_msgs::Marker m;
	      m.header.frame_id = "base_link";
	      m.header.stamp = ros::Time();
	      m.ns = ros::this_node::getName();
	      m.type = visualization_msgs::Marker::ARROW;
	      m.action = visualization_msgs::Marker::ADD;
	      m.pose.position.x = 0;
	      m.pose.position.y = 0;
	      m.pose.position.z = 0;
	      m.pose.orientation = ahrs.imu.orientation;
	      m.scale.x = 1;
	      m.scale.y = 0.25;
	      m.scale.z = 0.1;
	      m.color.a = 1.0;
	      m.color.r = 0.0;
	      m.color.g = 1.0;
	      m.color.b = 0.0;
	      d->vis_pub_.publish(m);
	    }
	  
	}
      else if (buf.compare(0,4,"AGp,") == 0)
	{
	  DEBUG("Got processed accelerometer and gyro message: " << buf);
	  std::vector<double> accel(3);
	  int n = sscanf(buf.c_str(),"AGp,%*d,%lf,%lf,%lf,%lf,%lf,%lf,%lf",
			 &accel[0],
			 &accel[1],
			 &accel[2],
			 &imu.angular_velocity.x, // [rad/s] @@@ what is the ROS axis convention?
			 &imu.angular_velocity.y,
			 &imu.angular_velocity.z,
			 &temperature); // [C] 
	  imu.linear_acceleration.x = accel[0] / 1000.0; // [g]
	  imu.linear_acceleration.y = accel[1] / 1000.0; // [g]
	  imu.linear_acceleration.z = accel[2] / 1000.0; // [g]
	  if (n != 7)
	    {
	      ERROR("Failed to parse accelerometer/gyro message!" << std::endl;);
	      return;
	    }
	  imu.header.stamp = bytes.ds_header.io_time; // not a ds_ message type.
	  d->imu_last = imu;
	  
	  d->imu_pub_.publish(imu);
	  updateTimestamp("imu", imu.header.stamp);
	  
	  // @@@ message does not include orientation, only rates and accelerations.
	  
	}
      else if (buf.compare(0,5,"magp,") == 0)
	{
	  DEBUG("Got processed magnetometer message: " << buf);
	  std::vector<double> magv(3);
	  int n = sscanf(buf.c_str(),"magp,%*d,%lf,%lf,%lf",
			 &magv[0], // mG
			 &magv[1],
			 &magv[2]);
	  mag.x = magv[0] * 0.0001 * 1.0e9; // nT
	  mag.y = magv[1] * 0.0001 * 1.0e9; // nT
	  mag.z = magv[2] * 0.0001 * 1.0e9; // nT
	  
	  mag.temperature = 0.0;
	  
	  mag.ds_header = bytes.ds_header;
	  FILL_SENSOR_HDR_IOTIME(mag, mag.ds_header.io_time);
	  d->mag_last = mag;
	  
	  d->mag_pub_.publish(mag);
	  updateTimestamp("mag", mag.header.stamp);
	  
	}
      else
	{
	  ERROR("Got unknown message (mask protocol): " << buf);
	  return;
	}
    }
  else if (d->protocol_ == "BitStreamAscii")
    {
      //DEBUG("Got bitstream ascii: " << buf);
      
      // Output is VID-ordered.  Packet parsing is hard-coded for the setup in this driver.
      double yaw_deg, yawt_deg, temperature_C;
      std::vector<double> accelp_mg(3);
      std::vector<double> magp_mG(3);
      std::vector<double> gyrop_radps(3); // @@@ units unconfirmed (undocumented).
      int n = sscanf(buf.c_str(),"$CUS0,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf",
		     &ahrs.pitch,
		     &ahrs.roll,
		     &yaw_deg,
		     &yawt_deg,
		     &magp_mG[0],
		     &magp_mG[1],
		     &magp_mG[2],
		     &accelp_mg[0],
		     &accelp_mg[1],
		     &accelp_mg[2],
		     &gyrop_radps[0],
		     &gyrop_radps[1],
		     &gyrop_radps[2],
		     &temperature_C);
      //if (n != 14)
	//{
	  //ERROR("Failed to parse BitStreamAscii message: " << buf << std::endl;);
	//  return;
	//}
      
      
      ahrs.compass.heading = yawt_deg;
      ahrs.compass.is_true_heading = true; // using yawt output.
      ahrs.compass.heading_covar = 0.0;

      // @@@ do these have to be converted into ROS frame?  Orientation for sure yes.
      ahrs.imu.linear_acceleration.x = accelp_mg[0] / 1000.0 * STDGRAVITY; // [m/s^2]
      ahrs.imu.linear_acceleration.y = accelp_mg[1] / 1000.0 * STDGRAVITY; // [m/s^2]
      ahrs.imu.linear_acceleration.z = accelp_mg[2] / 1000.0 * STDGRAVITY; // [m/s^2]
      ahrs.imu.linear_acceleration_covariance.fill(0.0);
      
      ahrs.imu.angular_velocity.x = gyrop_radps[0];
      ahrs.imu.angular_velocity.y = gyrop_radps[1];
      ahrs.imu.angular_velocity.z = gyrop_radps[2];
      ahrs.imu.angular_velocity_covariance.fill(0.0);
	
      ahrs.mag.x = magp_mG[0] * 0.0001 * 1.0e9; // nT
      ahrs.mag.y = magp_mG[1] * 0.0001 * 1.0e9; // nT
      ahrs.mag.z = magp_mG[2] * 0.0001 * 1.0e9; // nT
      ahrs.mag.field_covar.fill(0.0);
      
      ahrs.mag.temperature = temperature_C; // C
      ahrs.mag.temperature_covar = 0.0;
      
      // orientation in ROS convention.
      double yaw_ros = -(ahrs.compass.heading - 90.0);
      tf::Quaternion q = tf::createQuaternionFromRPY(ahrs.roll*M_PI/180.0,-ahrs.pitch*M_PI/180.0,yaw_ros*M_PI/180.0);
      tf::quaternionTFToMsg(q, ahrs.imu.orientation);

      // Timestamps
      ahrs.ds_header = bytes.ds_header;
      FILL_SENSOR_HDR_IOTIME(ahrs, ahrs.ds_header.io_time);
      FILL_SENSOR_HDR_IOTIME(ahrs.mag, ahrs.ds_header.io_time);
      FILL_SENSOR_HDR_IOTIME(ahrs.compass, ahrs.ds_header.io_time);
      ahrs.imu.header.stamp = ahrs.ds_header.io_time; // not a ds_sensor_msg.
      
      // Publish
      d->ahrs_pub_.publish(ahrs);
      updateTimestamp("ahrs", ahrs.header.stamp);
      
      // Publish data for visualization if configured to do so.
      // This gets published at the AHRS rate.
      if (d->publish_rviz_marker_)
	{
	  visualization_msgs::Marker m;
	  m.header.frame_id = "base_link";
	  m.header.stamp = ros::Time();
	  m.ns = ros::this_node::getName();
	  m.type = visualization_msgs::Marker::ARROW;
	  m.action = visualization_msgs::Marker::ADD;
	  m.pose.position.x = 0;
	  m.pose.position.y = 0;
	  m.pose.position.z = 0;
	  m.pose.orientation = ahrs.imu.orientation;
	  m.scale.x = 1;
	  m.scale.y = 0.25;
	  m.scale.z = 0.1;
	  m.color.a = 1.0;
	  m.color.r = 0.0;
	  m.color.g = 1.0;
	  m.color.b = 0.0;
	  d->vis_pub_.publish(m);
	}
	  

      
    }
  else
    {
      FATAL("Unknown protocol.");
    }
  
}




void SpartonM2::setup()
{

  SensorBase::setup();

  // Get some extra parameters.
  DS_D(SpartonM2);
  d->publish_rviz_marker_ = ros::param::param<bool>("~publish_rviz_marker",false);
  d->protocol_ = ros::param::param<std::string>("~protocol","BitStreamAscii");
  d->rate_hz_ = ros::param::param<int>("~rate_hz",10); // Ignored for "mask" protocol.
  auto latitude = ros::param::param<float>("~latitude",41.5);
  auto longitude = ros::param::param<float>("~longitude",-70.7);
  auto altitude = ros::param::param<float>("~altitude",0.0); 
  auto day = ros::param::param<float>("~day",2021.6);  
  
  // Set up publishers.
  d->ahrs_pub_ = nodeHandle().advertise<ds_sensor_msgs::Ahrs>(ros::this_node::getName() + "/ahrs", 1);
  if (d->protocol_ == "mask")
    {
      d->mag_pub_ = nodeHandle().advertise<ds_sensor_msgs::VectorMagneticField>(ros::this_node::getName() + "/mag", 1);
      d->imu_pub_ = nodeHandle().advertise<sensor_msgs::Imu>(ros::this_node::getName() + "/imu", 1);
    }
  else if (d->protocol_ == "BitStreamAscii") {}
  else
    {
      FATAL("Unknown protocol.");
    }

  if (d->publish_rviz_marker_)
    {
      d->vis_pub_ = nodeHandle().advertise<visualization_msgs::Marker>(ros::this_node::getName() + "/spartonm2_orientation",0);
    }

  // Configure the sensor open loop and then start it spitting.  The sensor talks in Northtek
  // and spits out an OK for commands it likes.  Sensor responses are ignored here.
  //
  // This is enough to configure the sensor from factory defaults to the mode we want.
  // This does not set or overwrite calibrations.  We assume those are done independently,
  // e.g., using the GUI provided with the sensor.
  

  // Reset the sensor's volatile settings.  (Does not alter calibration settings.)
  SpartonM2::SpartonSendCommand("reset");
  ros::Duration(1.0).sleep();

  // @@@ Other configurations???

  // Ensure output from all protocols supported by this driver is disabled.
  SpartonM2::SpartonSendCommand("magp_mask d.off");
  SpartonM2::SpartonSendCommand("accelGyrop_mask d.off");
  SpartonM2::SpartonSendCommand("compass_mask d.off");
  SpartonM2::SpartonSendCommand("chan0TriggerDivisor 0 set drop");
  SpartonM2::SpartonSendCommand("chan0TriggerDivisor 1 set drop");
  SpartonM2::SpartonSendCommand("chan0TriggerDivisor 2 set drop");

  // Do not configure anything to do with calibration, nor overwrite any stored
  // calibration or configuration values.  Also not saving any of the configuration
  // values above.


  // Set approximate latitude and longitude to use internal WMM.
  SpartonM2::SpartonSendCommand(std::string("lat f")  + std::to_string(latitude) + " set drop");
  SpartonM2::SpartonSendCommand(std::string("lonG f")  + std::to_string(longitude) + " set drop");
  SpartonM2::SpartonSendCommand(std::string("alt f")  + std::to_string(altitude) + " set drop");
  SpartonM2::SpartonSendCommand(std::string("day f")  + std::to_string(day) + " set drop");
  SpartonM2::SpartonSendCommand("set_wmm 1 set drop");
  
  // Basic data output using "mask" variables.  This will start the sensor spitting after
  // a power cycle.  It will do nothing if output was stopped manually by a CTRL-S.
  if ( d->protocol_ == "mask" )
    {
    
      // Turn on processed accel/gyro output
      SpartonM2::SpartonSendCommand("accelGyrop_mask d.on");
      
      // Turn on processed magnetometer output.
      SpartonM2::SpartonSendCommand("magp_mask d.on");
      
      // Turn on processed attitude output
      SpartonM2::SpartonSendCommand("compass_mask d.on");
      
    }
  // BitSteamAscii protocol.  This sets up custom ASCII output with everything of interest
  // in a single CRLF-terminated ASCII packet.
  else if ( d->protocol_ == "BitStreamAscii" )
    {
      // Configure protocol on chan0
      SpartonM2::SpartonSendCommand("chan0Format 3 set drop");
      
      // Configure chan0 to trigger based on the timer.
      SpartonM2::SpartonSendCommand("chan0Trigger 5 set drop");

      // Configure output to match desired rate.
      SpartonM2::SpartonSendCommand(std::string("chanTimerX100us ") + std::to_string(10000/d->rate_hz_) + " set drop");
      SpartonM2::SpartonSendCommand("chan0Trigger 6 set drop");
      SpartonM2::SpartonSendCommand("chan0TriggerDivisor 1 set drop");

      // Configure contents of custom output.
      SpartonM2::SpartonSendCommand("chan0Enables array[ 0 15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ]array set drop"); // clear
      SpartonM2::SpartonSendCommand("chan0EnableBit roll dvid@ set drop");  // roll, deg
      SpartonM2::SpartonSendCommand("chan0EnableBit pitch dvid@ set drop"); // pitch, deg
      SpartonM2::SpartonSendCommand("chan0EnableBit yaw dvid@ set drop"); // hdg, deg
      SpartonM2::SpartonSendCommand("chan0EnableBit yawt dvid@ set drop");  // hdg, true, deg
      SpartonM2::SpartonSendCommand("chan0EnableBit magp dvid@ set drop");  // processed magnetic vector (mG)
      SpartonM2::SpartonSendCommand("chan0EnableBit accelp dvid@ set drop"); // processed accelerations (mg)
      SpartonM2::SpartonSendCommand("chan0EnableBit gyrop dvid@ set drop"); // processed gyro (deg/s?)
      SpartonM2::SpartonSendCommand("chan0EnableBit temperature dvid@ set drop"); // temperature (C)
      
      // Manual suggests verifying VIDs because the output is in VID order and otherwise unlabeled, and
      // VIDs are not guaranteed constant between f/w changes.
      // @@@ Unimplemented.
      
      
    }
  
}
  
void SpartonM2::SpartonSendCommand(const std::string payload)
{

  auto ostream = std::ostringstream{};
  
  ostream << payload << "\r\n";  // extra won't hurt if already \r\n terminated.
  
  // send the command.
  DEBUG("Sending '" << ostream.str() << "' to instrument.");
  SensorBase::sendCommand(ostream.str(), "instrument");
  
}  
}
